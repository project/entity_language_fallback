# Entity Language Fallback

This module allow administrators to configure a prioritised list of fallback
languages per language. The fallback languages are used for entity view /
entity upcast.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/entity_language_fallback).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/entity_language_fallback).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

- Language Module


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1.After installation go to admin/config/regional/language, edit one of the
  languages and configure the fallback languages.


## Maintainers

- Jens Beltofte - [beltofte](https://drupal.org/u/beltofte)
- Valery Louri - [valthebald](https://www.drupal.org/u/valthebald)
