<?php

namespace Drupal\entity_language_fallback;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access check helper for the entity language fallback.
 */
class AccessHelper {

  /**
   * Static cache of entity access results, cached by entity type ID,
   * and then by entity ID.
   *
   * @var array
   *   $accessResults[$entityTypeId][$entityId]
   */
  protected static $accessResults = [];

  /**
   * Implements hook_entity_access().
   *
   * This method is needed because NodeAccessControlHandler (and probably access
   * handlers of other content entities) expect entities to have same language
   * as the content language of the page. For fallback entities, this may not be
   * the case.
   *
   * To fix, we check access of the fallback entity instead of requested entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Requested entity.
   * @param mixed $operation
   *   Operation to perform.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Access check result.
   */
  public static function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) : AccessResult {
    // Check if the entity is a translatable content entity.
    if (!($entity instanceof ContentEntityInterface)
      || !$entity->isTranslatable()
      || $entity->search_api_skip_tracking
      || $entity->isNew()
      || !empty($entity->preventLooping)) {
      return AccessResult::neutral();
    }
    $entityTypeId = $entity->getEntityTypeId();
    if (!isset(self::$accessResults[$entityTypeId])) {
      self::$accessResults[$entityTypeId] = [];
    }
    if (!isset(self::$accessResults[$entityTypeId][$entity->id()])) {
      if (\Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId()
        == $entity->language()->getId()) {
        self::$accessResults[$entityTypeId][$entity->id()] = AccessResult::neutral();
        return self::$accessResults[$entityTypeId][$entity->id()];
      }
      $entity->preventLooping = TRUE;
      self::$accessResults[$entityTypeId][$entity->id()] = $entity->access($operation, $account, TRUE);
      unset($entity->preventLooping);
    }
    return self::$accessResults[$entityTypeId][$entity->id()];
  }

}
