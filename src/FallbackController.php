<?php

namespace Drupal\entity_language_fallback;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Controller with all fallback related logic.
 */
class FallbackController implements FallbackControllerInterface {

  /**
   * Simple list of fallback language codes.
   *
   * @var array
   */
  protected $fallbackChain;

  /**
   * List of entity bundle candidates.
   *
   * @var array
   */
  protected $entityBundleCandidates;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * TranslationJobHandler constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   An instance of the Language management service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   An instance of entity type manager service.
   */
  public function __construct(
    LanguageManagerInterface $languageManager,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->languageManager = $languageManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->fallbackChain = [];
    $this->entityBundleCandidates = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackChain($lang_code) {
    $this->ensureFallbackChain($lang_code);
    return $this->fallbackChain[$lang_code];
  }

  /**
   * {@inheritdoc}
   *
   * @todo Consider using @cache.memory service for cache meta-data (8.6+).
   */
  public function getEntityFallbackCandidates(ContentEntityInterface $entity, $language_code) {
    // Result of this can change by entity + bundle combination.
    $cid = "{$entity->getEntityType()->id()}:{$entity->bundle()}";
    if (isset($this->entityBundleCandidates[$cid])) {
      return $this->entityBundleCandidates[$cid];
    }

    // Generate if the value is missing.
    $candidates = [];

    if ($entity->isTranslatable()) {
      $candidates[$language_code] = $language_code;

      foreach ($this->getFallbackChain($language_code) as $fallback_langcode) {
        if (!(empty($fallback_langcode) || isset($candidates[$fallback_langcode]))) {
          $candidates[$fallback_langcode] = $fallback_langcode;
        }
      }
    }

    $this->entityBundleCandidates[$cid] = $candidates;
    return $candidates;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslation($lang_code, ContentEntityInterface $entity) {
    $this->ensureFallbackChain($lang_code);

    foreach ($this->fallbackChain[$lang_code] as $candidate) {
      if ($entity->hasTranslation($candidate)) {
        return $entity->getTranslation($candidate);
      }
    }

    return FALSE;
  }

  /**
   * Populate internal fallback chain information if necessary.
   */
  protected function ensureFallbackChain($lang_code) {
    if (isset($this->fallbackChain[$lang_code])) {
      return;
    }

    $this->fallbackChain[$lang_code] = ($language = ConfigurableLanguage::load($lang_code))
      ? array_filter($language->getThirdPartySetting('entity_language_fallback', 'fallback_langcodes', []))
      : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslations(ContentEntityInterface $entity) {
    $translations = [];
    foreach ($this->languageManager->getLanguages() as $langcode => $language) {
      if ($entity->hasTranslation($langcode)) {
        $translations[$langcode] = $entity->getTranslation($langcode);
      }
      elseif ($fallback = $this->getTranslation($langcode, $entity)) {
        $translations[$langcode] = $fallback;
      }
    }
    return $translations;
  }

}
