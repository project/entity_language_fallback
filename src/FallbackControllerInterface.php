<?php

namespace Drupal\entity_language_fallback;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Controller interface with all fallback related logic.
 */
interface FallbackControllerInterface {

  /**
   * Get fallback chain for a specific langauge code.
   *
   * @param string $lang_code
   *   The language code to find the fallback chain for.
   *
   * @return array
   *   Language codes for the fallback chain, most preferred languages first.
   */
  public function getFallbackChain($lang_code);

  /**
   * Provides the list of language candidates for a given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity we are checking for.
   * @param string $language_code
   *   The language we have requested.
   *
   * @return array
   *   List of language fallback candidates for the language for the entity.
   */
  public function getEntityFallbackCandidates(ContentEntityInterface $entity, $language_code);

  /**
   * Get the fallback translation for a specific entity.
   *
   * @param string $lang_code
   *   The language code.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity to find the translation for.
   *
   * @return mixed
   *   The fallback entity translation, or FALSE if nothing was found.
   */
  public function getTranslation($lang_code, ContentEntityInterface $entity);

  /**
   * Get list of translations for a specific entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity to find the translations for.
   *
   * @return array
   *   Array of entity translations, including fallback content.
   */
  public function getTranslations(ContentEntityInterface $entity);

}
